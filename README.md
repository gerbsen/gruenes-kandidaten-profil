# Grünes Kandidaten Profil

Das **Ziel** dieses Tools soll eine möglichst hübsche und informative Webseite für die einzelnen Kandidaten zur Kommunalwahl sein.
Die Seiten werden mit dem statischem Webseiten-Generator [Jekyll](https://jekyllrb.com/) automatisch erstellt.
Erste Erfahrungen wurden bei einem [anderen Projekt](https://gitlab.com/gerbsen/kis) bereits erfolgreich gesammelt.
Die Daten für die einzelnen Kandidaten werden durch eine selbsterstellte Applikation eingesammelt.
Das Design der Webseite **muss** sich an der Kampagne zur Kommunalwahl beziehungsweise dem Design des Bundesvorstands orientieren.

## Die Kandidaten und Kandidatinnen

Die gewählten Spitzenkandidat*innen sind:

- Wahlkreis 0:
  1. Katharina Krefft
  2. Tim Elschner
  3. Franka Moritz
- Wahlkreis 1:
  1. Tobias Peter
  2. Christin Melcher
  3. Tizian Optenberg
- Wahlkreis 2:
  1. Jürgen Kasek
  2. Marcel Pruß
- Wahlkreis 3:
  1. Stefanie Gruner
  2. Martin Meißner
  3. Anne Sehl
- Wahlkreis 4:
  1. Gesine Märtens
  2. Norman Volger
  3. Sophia Kraft
- Wahlkreis 5:
  1. Michael Schmidt
  2. Kristina Weyh
- Wahlkreis 6:
  1. Konstantin Richter
  2. Claudia Fülle
- Wahlkreis 7:
  1. Petra Cagalj-Sejdi
  2. Volker Holzendorf
  3. Anna Kaleri
- Wahlkreis 8:
  1. Annette Körner
  2. Lutz Unbekannt
  3. Michael Plättner
- Wahlkreis 9:
  1. Martin Biederstedt
  2. Marcus Korzer

### Videos

Videos wurden bisher von Martin Jehnichen (Netzbegrünung e.V.) während des letzten Stadtparteitages aufgenommen und in kleine Stückchen geschnitten.
Diese Videos können auf der [Youtube-Seite](https://www.youtube.com/user/GrueneLeipzig/videos) des KV Leipzig eingesehen werden.

## Schema für eine*n Kandidat*in

```
Person:
   Name 
   Vorname
   Profilbild
   Lebt im Ortsteil
   Alter
   Kinder
   Beruf
   Ausbildung (mit Character Limit)
   Familienstand

Kontakt
   Twitter
   Instagram
   Facebook
   Website
   Email
   Telefonnummer

Beschreibung
   Youtube-Video-Link:
   Youtube-Video-Beschreibung:
   Ich-in-140-Zeichen
   Beschreibung

Wahlen
   Wahlkreis
   Listenplatz

Tätigkeiten[]
	- von, bis, beschreibung, link
Mitgliedschaften[] (Ausschüsse, Vereine)
	- von, bis, beschreibung, link

Sonstiges:
   Verrücktes (Hobbies, Was niemand über dich kennt)
   mein Lieblingsort in Leipzig
```

### Zukünftiges 
- Zeitungsartikel (weiterführende Links)
- meine bisherigen Erfolge in der Kommunalpolitik


## Design

Das Design ist mit Absicht sehr schlicht gehalten und orientiert sich an der Europa- und Kommunalwahlkampagne des Bundesverbandes.
Damit kann sichergestellt werden, dass die Webseite überall hinpasst und alles homogen wirkt.
Weiter Designhinweise/vorgaben gibt's beim [Corporate Design](https://www.gruene.de/fileadmin/user_upload/Dokumente/GRUENE_Design_Handbuch_Januar2017.pdf) Handbuch des Bundesverbandes.

## Development

Die Entwicklung an diesem Tool erfolgt mittels [`gitflow`](https://nvie.com/posts/a-successful-git-branching-model/).
Neue Features werden in Feature-Branches entwickelt und dann nach einem Code-Review in den `develop`-Branch mittels Merge Request eingespielt.

### Kandidatin-Webseite

Vor dem Start der Webseite muss eine Kandidaten-Datei erstellt werden:

```bash
cp server/_data/candidates.default.yml server/_data/candidates.yml
```

Die Webseite kann mit folgendem Befehl lokal gestartet werden:

```bash
bundle install
bundle exec jekyll serve
```

Die Webseite ist dann unter [http://localhost:4000](http://localhost:4000) erreichbar.

### Kandidaten-Fragebogen

Vor dem Start muss eine Konfigurationsdatei erstellt werden:

```bash
cp server/config.default.js server/config.js
cp server/users.default.csv server/users.csv
```

Der Server selbst wird wie folgt gestartet:

```bash
cd server
npm install
nodemon start.js
```

Der Server ist unter [http://localhost:8000/](http://localhost:8000/) erreichbar.\
Die voreingestellten Anmeldedaten lauten:

- Nutzername: `erika-mustermann`
- Passwort: `erika-mustermann`

#### Formular

Für das Formular wird [form.io](http://formio.github.io/formio.js/) verwendet.

## Deployment

Aktuell gibt es eine `development` und eine `production` Umgebung:

- [Development](https://kandidaten-staging.ichbinmitdergesamtsituationunzufrieden.de)
- [Production](https://kandidaten.ichbinmitdergesamtsituationunzufrieden.de)

### Docker

Da wir nur statische HTML Dateien erzeugen, können wir diese einfach über einen minimalen NGINX Container ausrollen.
Diesen Container starten wir in der `production`-Umgebung mittels:

#### Production

```bash
docker run -d -p 8011:4000 --restart always --name kommunalwahl_leipzig_prod -v /var/kommunalwahl_leipzig/uploads_prod/:/usr/src/app/_data registry.gitlab.com/gerbsen/gruenes-kandidaten-profil:master
```

#### Development

Für die `development`-Umgebung starten wir den Container so:

```bash
docker run -d -p 8012:4000 \
   --restart always --name kommunalwahl_leipzig_dev \
   -v /var/kommunalwahl_leipzig/develop/_data:/usr/src/app/_data \
   registry.gitlab.com/gerbsen/gruenes-kandidaten-profil:develop
```

Die Container werden aktuell auf einem privaten Server ausgerollt.
Bitte auf die Port-Angaben achten, da diese sich durchaus auf einem anderen Server unterscheiden können.

Mit [**WatchTower**](https://github.com/v2tec/watchtower) werden neu gebaute Images überwacht und automatisch aktualisiert:

```bash
docker run -d \
  --name watchtower \
  --restart always \
  -v /var/run/docker.sock:/var/run/docker.sock \
  v2tec/watchtower kommunalwahl_leipzig_prod kommunalwahl_leipzig_dev --interval 30 --debug
```
