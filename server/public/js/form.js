var person = require("./person");
var kontakt = require("./kontakt");
var beschreibung = require("./beschreibung");
var wahlen = require("./wahlen");
var taetigkeiten = require("./taetigkeiten");
var mitgliedschaften = require("./mitgliedschaften");
var sonstiges = require("./sonstiges");

var form = {
    "display": "form",
    "components": [
        {
            "label": "Persönliches",
            "legend": "Persönliches",
            "mask": false,
            "tableView": true,
            "alwaysEnabled": false,
            "type": "fieldset",
            "input": false,
            "key": "fieldSet",
            "components": person
        },
        {
            "label": "Kontakt",
            "legend": "Kontakt",
            "mask": false,
            "tableView": true,
            "alwaysEnabled": false,
            "type": "fieldset",
            "input": false,
            "key": "fieldSet",
            "components": kontakt
        },
        {
            "label": "Beschreibung",
            "legend": "Beschreibung",
            "mask": false,
            "tableView": true,
            "alwaysEnabled": false,
            "type": "fieldset",
            "input": false,
            "key": "fieldSet",
            "components": beschreibung
        },
        {
            "label": "Wahlen",
            "legend": "Wahlen",
            "mask": false,
            "tableView": true,
            "alwaysEnabled": false,
            "type": "fieldset",
            "input": false,
            "key": "fieldSet",
            "components": wahlen
        },
        {
            "label": "sonstiges",
            "legend": "Sonstiges",
            "mask": false,
            "tableView": true,
            "alwaysEnabled": false,
            "type": "fieldset",
            "input": false,
            "key": "fieldSet",
            "components": sonstiges
        },
        {
            "label": "Tätigkeiten",
            "legend": "Tätigkeiten",
            "mask": false,
            "tableView": true,
            "alwaysEnabled": false,
            "type": "fieldset",
            "input": false,
            "key": "fieldSet",
            "components": taetigkeiten
        },
        {
            "label": "mitgliedschaften",
            "legend": "Mitgliedschaften",
            "mask": false,
            "tableView": true,
            "alwaysEnabled": false,
            "type": "fieldset",
            "input": false,
            "key": "fieldSet",
            "components": mitgliedschaften
        },
        {
            "type": "button",
            "label": "Submit",
            "key": "submit",
            "disableOnInvalid": true,
            "theme": "primary",
            "input": true,
            "tableView": true
        }
    ],
    "settings": {
        "pdf": {
            "id": "1ec0f8ee-6685-5d98-a847-26f67b67d6f0",
            "src": "https://files.form.io/pdf/5692b91fd1028f01000407e3/file/1ec0f8ee-6685-5d98-a847-26f67b67d6f0"
        }
    }
}

module.exports = form;