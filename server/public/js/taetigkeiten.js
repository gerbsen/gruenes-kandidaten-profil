var taetigkeiten = [{
    label: '',
    key: 'taetigkeiten',
    type: 'datagrid',
    input: true,
    validate: {
      minLength: 1,
      maxLength: 15
    },
    components: [
      {
        "label": "Von",
        "description": "Ein Jahr.",
        "mask": false,
        "tableView": true,
        "alwaysEnabled": false,
        "type": "number",
        "input": true,
        "key": "von",
        "validate": {
            "customMessage": "",
            "json": "",
            "max": 2019,
            "min": 1900
        },
        "delimiter": false,
        "requireDecimal": false,
        "encrypted": false
      },
      {
        "label": "Bis",
        "description": "Ein Jahr.",
        "mask": false,
        "tableView": true,
        "alwaysEnabled": false,
        "type": "number",
        "input": true,
        "key": "bis",
        "validate": {
            "customMessage": "",
            "json": "",
            "max": 2019,
            "min": 1900
        },
        "delimiter": false,
        "requireDecimal": false,
        "encrypted": false
      },
      {
        "label": "Beschreibung",
        "description": "Was genau hast du getan?",
        "allowMultipleMasks": false,
        "showWordCount": false,
        "showCharCount": false,
        "tableView": true,
        "alwaysEnabled": false,
        "type": "textfield",
        "input": true,
        "key": "beschreibung",
        "widget": {
            "type": ""
        }
      },
      {
        "label": "Link",
        "description": "Hast du einen Link zur Tätigkeit?",
        "allowMultipleMasks": false,
        "showWordCount": false,
        "showCharCount": false,
        "tableView": true,
        "alwaysEnabled": false,
        "type": "url",
        "input": true,
        "key": "link"
      }
    ]
}]

module.exports = taetigkeiten;