var person = [
    {
        "label": "Titel",
        "allowMultipleMasks": false,
        "showWordCount": false,
        "showCharCount": false,
        "tableView": true,
        "alwaysEnabled": false,
        "type": "textfield",
        "input": true,
        "key": "titel",
        "defaultValue": "",
        "validate": {
            "required": true,
            "customMessage": "",
            "json": "",
            "maxLength": 64
        },
        "conditional": {
            "show": "",
            "when": "",
            "json": ""
        },
        "inputFormat": "plain",
        "encrypted": false,
        "properties": {},
        "tags": [],
        "customConditional": "",
        "logic": []
    },
    {
        "label": "Vorname",
        "allowMultipleMasks": false,
        "showWordCount": false,
        "showCharCount": false,
        "tableView": true,
        "alwaysEnabled": false,
        "type": "textfield",
        "input": true,
        "key": "vorname",
        "defaultValue": "",
        "validate": {
            "required": true,
            "customMessage": "",
            "json": "",
            "maxLength": 64
        },
        "conditional": {
            "show": "",
            "when": "",
            "json": ""
        },
        "inputFormat": "plain",
        "encrypted": false,
        "properties": {},
        "tags": [],
        "customConditional": "",
        "logic": []
    },
    {
        "label": "Nachname",
        "allowMultipleMasks": false,
        "showWordCount": false,
        "showCharCount": false,
        "tableView": true,
        "alwaysEnabled": false,
        "type": "textfield",
        "input": true,
        "key": "nachname",
        "defaultValue": "",
        "validate": {
            "required": true,
            "customMessage": "",
            "json": "",
            "maxLength": 64
        },
        "conditional": {
            "show": "",
            "when": "",
            "json": ""
        },
        "inputFormat": "plain",
        "encrypted": false,
        "properties": {},
        "tags": [],
        "customConditional": "",
        "logic": [],
        "widget": {
            "type": ""
        }
    },
    {
        "label": "Profilfoto",
        "mask": false,
        "tableView": true,
        "alwaysEnabled": false,
        "type": "file",
        "input": true,
        "key": "profilfoto",
        "defaultValue": [],
        "validate": {
            "required": true,
            "customMessage": "",
            "json": ""
        },
        "image": true,
        "fileMaxSize": "500kb",
        "storage": "base64",
        "dir": "",
        "webcam": false,
        "fileTypes": [
            {
                "label": "Bilder",
                "value": "jpeg"
            },
            {
                "label": "Bilder",
                "value": "jpg"
            },
            {
                "label": "Bilder",
                "value": "png"
            }
        ],
        "imageSize": "",
        "encrypted": false
    },
    {
        "label": "Stadtteil",
        "description": "Der Stadtteil in dem du wohnst.",
        "mask": false,
        "tableView": true,
        "alwaysEnabled": false,
        "type": "select",
        "input": true,
        "key": "stadtteil",
        "defaultValue": "",
        "validate": {
            "required": true,
            "customMessage": "",
            "json": ""
        },
        "conditional": {
            "show": "",
            "when": "",
            "json": ""
        },
        "data": {
            "values": [
                { "label" : "Zentrum", "value" : "Zentrum" },
                { "label" : "Zentrum-Ost", "value" : "Zentrum-Ost" },
                { "label" : "Zentrum-Südost", "value" : "Zentrum-Südost" },
                { "label" : "Zentrum-Süd", "value" : "Zentrum-Süd" },
                { "label" : "Zentrum-West", "value" : "Zentrum-West" },
                { "label" : "Zentrum-Nordwest", "value" : "Zentrum-Nordwest" },
                { "label" : "Zentrum-Nord", "value" : "Zentrum-Nord" },
                { "label" : "Schönefeld-Abtnaundorf", "value" : "Schönefeld-Abtnaundorf" },
                { "label" : "Schönefeld-Ost", "value" : "Schönefeld-Ost" },
                { "label" : "Mockau-Süd", "value" : "Mockau-Süd" },
                { "label" : "Mockau-Nord", "value" : "Mockau-Nord" },
                { "label" : "Thekla", "value" : "Thekla" },
                { "label" : "Plaußig-Portitz", "value" : "Plaußig-Portitz" },
                { "label" : "Neustadt-Neuschönefeld", "value" : "Neustadt-Neuschönefeld" },
                { "label" : "Volkmarsdorf", "value" : "Volkmarsdorf" },
                { "label" : "Anger-Crottendorf", "value" : "Anger-Crottendorf" },
                { "label" : "Sellerhausen-Stünz", "value" : "Sellerhausen-Stünz" },
                { "label" : "Paunsdorf", "value" : "Paunsdorf" },
                { "label" : "Heiterblick", "value" : "Heiterblick" },
                { "label" : "Mölkau", "value" : "Mölkau" },
                { "label" : "Engelsdorf", "value" : "Engelsdorf" },
                { "label" : "Baalsdorf", "value" : "Baalsdorf" },
                { "label" : "Althen-Kleinpösna", "value" : "Althen-Kleinpösna" },
                { "label" : "Reudnitz-Thonberg", "value" : "Reudnitz-Thonberg" },
                { "label" : "Stötteritz", "value" : "Stötteritz" },
                { "label" : "Probstheida", "value" : "Probstheida" },
                { "label" : "Meusdorf", "value" : "Meusdorf" },
                { "label" : "Liebertwolkwitz", "value" : "Liebertwolkwitz" },
                { "label" : "Holzhausen", "value" : "Holzhausen" },
                { "label" : "Südvorstadt", "value" : "Südvorstadt" },
                { "label" : "Connewitz", "value" : "Connewitz" },
                { "label" : "Marienbrunn", "value" : "Marienbrunn" },
                { "label" : "Lößnig", "value" : "Lößnig" },
                { "label" : "Dölitz-Dösen", "value" : "Dölitz-Dösen" },
                { "label" : "Schleußig", "value" : "Schleußig" },
                { "label" : "Plagwitz", "value" : "Plagwitz" },
                { "label" : "Kleinzschocher", "value" : "Kleinzschocher" },
                { "label" : "Großzschocher", "value" : "Großzschocher" },
                { "label" : "Knautkleeberg-Knauthain", "value" : "Knautkleeberg-Knauthain" },
                { "label" : "Hartmannsdorf-Knautnaundorf", "value" : "Hartmannsdorf-Knautnaundorf" },
                { "label" : "Schönau", "value" : "Schönau" },
                { "label" : "Grünau-Ost", "value" : "Grünau-Ost" },
                { "label" : "Grünau-Mitte", "value" : "Grünau-Mitte" },
                { "label" : "Grünau-Siedlung", "value" : "Grünau-Siedlung" },
                { "label" : "Lausen-Grünau", "value" : "Lausen-Grünau" },
                { "label" : "Grünau-Nord", "value" : "Grünau-Nord" },
                { "label" : "Miltitz", "value" : "Miltitz" },
                { "label" : "Lindenau", "value" : "Lindenau" },
                { "label" : "Altlindenau", "value" : "Altlindenau" },
                { "label" : "Neulindenau", "value" : "Neulindenau" },
                { "label" : "Leutzsch", "value" : "Leutzsch" },
                { "label" : "Böhlitz-Ehrenberg", "value" : "Böhlitz-Ehrenberg" },
                { "label" : "Burghausen-Rückmarsdorf", "value" : "Burghausen-Rückmarsdorf" },
                { "label" : "Möckern", "value" : "Möckern" },
                { "label" : "Wahren", "value" : "Wahren" },
                { "label" : "Lützschena-Stahmeln", "value" : "Lützschena-Stahmeln" },
                { "label" : "Lindenthal", "value" : "Lindenthal" },
                { "label" : "Gohlis-Süd", "value" : "Gohlis-Süd" },
                { "label" : "Gohlis-Mitte", "value" : "Gohlis-Mitte" },
                { "label" : "Gohlis-Nord", "value" : "Gohlis-Nord" },
                { "label" : "Eutritzsch", "value" : "Eutritzsch" },
                { "label" : "Seehausen", "value" : "Seehausen" },
                { "label" : "Wiederitzsch", "value" : "Wiederitzsch" }
            ]
        },
        "encrypted": false,
        "valueProperty": "value",
        "properties": {},
        "tags": [],
        "customConditional": "",
        "logic": []
    },
    {
        "label": "Alter",
        "description": "Dein Alter in Jahren.",
        "mask": false,
        "tableView": true,
        "alwaysEnabled": false,
        "type": "number",
        "input": true,
        "key": "alter",
        "validate": {
            "customMessage": "",
            "json": "",
            "min": 18,
            "max": 100
        },
        "conditional": {
            "show": "",
            "when": "",
            "json": ""
        },
        "delimiter": false,
        "requireDecimal": false,
        "encrypted": false,
        "properties": {},
        "tags": [],
        "customConditional": "",
        "logic": []
    },
    {
        "label": "Ausbildung",
        "description": "Was Ausbildung hast du genossen?",
        "allowMultipleMasks": false,
        "showWordCount": false,
        "showCharCount": false,
        "tableView": true,
        "alwaysEnabled": false,
        "type": "textfield",
        "input": true,
        "key": "ausbildung",
        "defaultValue": "",
        "validate": {
            "customMessage": "",
            "json": "",
            "maxLength": 200
        },
        "inputFormat": "plain",
        "encrypted": false,
        "widget": {
            "type": ""
        }
    },
    {
        "label": "Beruf",
        "description": "Was machst du tagsüber?",
        "allowMultipleMasks": false,
        "showWordCount": false,
        "showCharCount": false,
        "tableView": true,
        "alwaysEnabled": false,
        "type": "textfield",
        "input": true,
        "key": "beruf",
        "defaultValue": "",
        "validate": {
            "customMessage": "",
            "json": "",
            "maxLength": 200
        },
        "inputFormat": "plain",
        "encrypted": false
    },
    {
        "label": "Familienstand",
        "allowMultipleMasks": false,
        "showWordCount": false,
        "showCharCount": false,
        "tableView": true,
        "alwaysEnabled": false,
        "type": "textfield",
        "input": true,
        "key": "familienstand",
        "defaultValue": "",
        "validate": {
            "customMessage": "",
            "json": "",
            "maxLength": 100
        },
        "inputFormat": "plain",
        "encrypted": false
    },
    {
        "label": "Kinder",
        "description": "Wieviele Kinder hast du?",
        "mask": false,
        "tableView": true,
        "alwaysEnabled": false,
        "type": "select",
        "input": true,
        "key": "kinder",
        "defaultValue": "",
        "data": {
            "values": [
                {
                    "label": "0",
                    "value": "0"
                },
                {
                    "label": "1",
                    "value": "1"
                },
                {
                    "label": "2",
                    "value": "2"
                },
                {
                    "label": "3",
                    "value": "3"
                },
                {
                    "label": "4",
                    "value": "4"
                },
                {
                    "label": "5",
                    "value": "5"
                },
                {
                    "label": "6",
                    "value": "6"
                }
            ]
        },
        "encrypted": false,
        "valueProperty": "value"
    },
    {
        "label": "Konfession",
        "showWordCount": false,
        "showCharCount": false,
        "tableView": true,
        "alwaysEnabled": false,
        "type": "textfield",
        "input": true,
        "key": "konfession",
        "widget": {
            "type": ""
        }
    }
]

module.exports = person;