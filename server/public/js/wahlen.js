var wahlen = [{
        "label": "Mitglied seit",
        "description": "Falls du Mitglied bist, die Anzahl der Jahre.",
        "mask": false,
        "tableView": true,
        "alwaysEnabled": false,
        "type": "number",
        "input": true,
        "key": "mitgliedSeit"
    },
    {
        "label": "Wahlkreis",
        "description": "Die Nummer des Wahlkreises",
        "mask": false,
        "tableView": true,
        "alwaysEnabled": false,
        "type": "select",
        "input": true,
        "key": "wahlkreis",
        "defaultValue": "",
        "data": {
            "values": [
                {
                    "label": "0",
                    "value": "0"
                },
                {
                    "label": "1",
                    "value": "1"
                },
                {
                    "label": "2",
                    "value": "2"
                },
                {
                    "label": "3",
                    "value": "3"
                },
                {
                    "label": "4",
                    "value": "4"
                },
                {
                    "label": "5",
                    "value": "5"
                },
                {
                    "label": "6",
                    "value": "6"
                },
                {
                    "label": "7",
                    "value": "7"
                },
                {
                    "label": "8",
                    "value": "8"
                },
                {
                    "label": "9",
                    "value": "9"
                }
            ]
        },
        "encrypted": false,
        "valueProperty": "value",
        "validate": {
            "required": true,
            "customMessage": "",
            "json": ""
        }
    },
    {
        "label": "Listenplatz",
        "description": "Auf welchem Listenplatz kandidierst du?",
        "mask": false,
        "tableView": true,
        "alwaysEnabled": false,
        "type": "select",
        "input": true,
        "key": "listenplatz",
        "defaultValue": "",
        "validate": {
            "required": true,
            "customMessage": "",
            "json": ""
        },
        "data": {
            "values": [
                {
                    "label": "1",
                    "value": "1"
                },
                {
                    "label": "2",
                    "value": "2"
                },
                {
                    "label": "3",
                    "value": "3"
                },
                {
                    "label": "4",
                    "value": "4"
                },
                {
                    "label": "5",
                    "value": "5"
                },
                {
                    "label": "6",
                    "value": "6"
                },
                {
                    "label": "7",
                    "value": "7"
                },
                {
                    "label": "8",
                    "value": "8"
                },
                {
                    "label": "9",
                    "value": "9"
                },
                {
                    "label": "10",
                    "value": "10"
                },
                {
                    "label": "11",
                    "value": "11"
                }
            ]
        },
        "encrypted": false,
        "valueProperty": "value"
    }
]

module.exports = wahlen;