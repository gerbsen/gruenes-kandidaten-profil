var beschreibung = [
    {
        "label": "Beschreibung zum YouTube-Video",
        "placeholder": "Das wollte ich schon immer mal sagen!",
        "description": "Du kannst noch eine kurze Beschreibung zu deinem YouTube-Bewerbungsvideo angeben.",
        "allowMultipleMasks": false,
        "showWordCount": false,
        "showCharCount": false,
        "tableView": true,
        "alwaysEnabled": false,
        "type": "textfield",
        "input": true,
        "key": "beschreibungZumYouTubeVideo",
        "defaultValue": "",
        "validate": {
            "customMessage": "",
            "json": "",
            "maxLength": 1337
        },
        "inputFormat": "plain",
        "encrypted": false
    },
    {
        "label": "Link zum YouTube-Video",
        "placeholder": "Beispiel: https://www.youtube.com/watch?v=4c-QELyr4vU",
        "description": "Dein Vorstellungsvideo von der Kreisversammlung. Du findest es <a href=\"https://www.youtube.com/user/GrueneLeipzig/\">hier</a>",
        "allowMultipleMasks": false,
        "showWordCount": false,
        "showCharCount": false,
        "tableView": true,
        "alwaysEnabled": false,
        "type": "url",
        "input": true,
        "key": "linkZumYouTubeVideo",
        "defaultValue": "",
        "validate": {
            "customMessage": "",
            "json": ""
        },
        "conditional": {
            "show": "",
            "when": "",
            "json": ""
        },
        "inputFormat": "plain",
        "encrypted": false,
        "properties": {},
        "tags": [],
        "customConditional": "",
        "logic": []
    },
    {
        "label": "Du in 15 Wörtern",
        "description": "Wer bist du? Kurz und knackig in einem Satz.",
        "showWordCount": false,
        "showCharCount": false,
        "tableView": true,
        "alwaysEnabled": false,
        "type": "textarea",
        "input": true,
        "key": "duIn15Wortern"
    },
    {
        "label": "Beschreibung",
        "description": "Was hast du vor?",
        "showWordCount": false,
        "showCharCount": false,
        "tableView": true,
        "alwaysEnabled": false,
        "type": "textarea",
        "input": true,
        "key": "beschreibung",
        "defaultValue": "",
        "validate": {
            "required": true,
            "customMessage": "",
            "json": "",
            "maxLength": 2000
        },
        "inputFormat": "plain",
        "encrypted": false,
        "rows": 10
    }
]

module.exports = beschreibung;