var kontakt = [
    {
        "label": "Email",
        "description": "Kontakt im Wahlkampf über diese Emailadresse.",
        "tableView": true,
        "alwaysEnabled": false,
        "type": "email",
        "input": true,
        "key": "email",
        "validate": {
            "required": true,
            "customMessage": "",
            "json": ""
        },
        "conditional": {
            "show": "",
            "when": "",
            "json": ""
        },
        "properties": {},
        "tags": [],
        "customConditional": ""
    },
    {
        "label": "Telefonnummer",
        "description": "Falls du das anbieten möchtest.",
        "showWordCount": false,
        "showCharCount": false,
        "tableView": true,
        "alwaysEnabled": false,
        "type": "phoneNumber",
        "input": true,
        "key": "telefonnummer",
        "defaultValue": "(034) 121-5593",
        "validate": {
            "customMessage": "",
            "json": ""
        },
        "conditional": {
            "show": "",
            "when": "",
            "json": ""
        },
        "inputFormat": "plain",
        "encrypted": false,
        "properties": {},
        "tags": [],
        "customConditional": "",
        "logic": []
    },
    {
        "label": "Webseite",
        "description": "Der Link zu deiner Webseite.",
        "allowMultipleMasks": false,
        "showWordCount": false,
        "showCharCount": false,
        "tableView": true,
        "alwaysEnabled": false,
        "type": "url",
        "input": true,
        "key": "website"
    },
    {
        "label": "Twitter",
        "description": "Der Link zu deinem Twitterprofil.",
        "allowMultipleMasks": false,
        "showWordCount": false,
        "showCharCount": false,
        "tableView": true,
        "alwaysEnabled": false,
        "type": "url",
        "input": true,
        "key": "twitter"
    },
    {
        "label": "Instagram",
        "description": "Der Link zu deinem Instagramprofil.",
        "allowMultipleMasks": false,
        "showWordCount": false,
        "showCharCount": false,
        "tableView": true,
        "alwaysEnabled": false,
        "type": "url",
        "input": true,
        "key": "instagram"
    },
    {
        "label": "Facebook",
        "description": "Der Link zu deinem Facebookprofil.",
        "allowMultipleMasks": false,
        "showWordCount": false,
        "showCharCount": false,
        "tableView": true,
        "alwaysEnabled": false,
        "type": "url",
        "input": true,
        "key": "facebook"
    }
];

module.exports = kontakt;