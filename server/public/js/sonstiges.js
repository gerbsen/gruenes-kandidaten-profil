var sonstiges = [
    {
        "label": "Verrücktes",
        "description": "Dinge die niemand über dich weiß.",
        "showWordCount": false,
        "showCharCount": false,
        "tableView": true,
        "alwaysEnabled": false,
        "type": "textarea",
        "input": true,
        "key": "verruecktes",
        "defaultValue": "",
        "validate": {
            "required": true,
            "customMessage": "",
            "json": "",
            "maxLength": 250
        },
        "inputFormat": "plain",
        "encrypted": false
    },
    {
        "label": "Lieblingsort",
        "description": "Dein Lieblingsort in Leipzig oder auch woanders.",
        "showWordCount": false,
        "showCharCount": false,
        "tableView": true,
        "alwaysEnabled": false,
        "type": "textfield",
        "input": true,
        "key": "lieblingsort",
        "widget": {
            "type": ""
        }
    }
]

module.exports = sonstiges;