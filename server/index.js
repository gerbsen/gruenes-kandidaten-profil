const express    = require('express');
const bodyParser = require('body-parser');
const app        = express();
const port       = 8000;
const path       = require('path');
const fs         = require('fs');
const basicAuth  = require('express-basic-auth')
const parse      = require('csv-parse');
const YAML       = require('yamljs')
const config     = require('./config');
const form       = require('./public/js/form');

const users = {};
fs.createReadStream("users.csv")
  .pipe(parse({ delimiter : ','}))
  .on('data', function(csvrow) {
      users[csvrow[0]] = csvrow[1];
  });

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({limit:'5mb'}));
app.use(bodyParser.json());
app.use(basicAuth({
    users: users, challenge : true
}))

app.get('/', function(req, res) {

  console.log("getting root html page");

  fs.readFile(path.join(__dirname + '/upload.html'), 'utf8', function (err, data) {
    if (err) {
      console.log(err);
    }
    else {
      data = data.replace(/CONTEXT_PATH/g, config.contextPath)
      data = data.replace(/FORM_PLACEHOLDER/g, JSON.stringify(form))
      res.end(data);
    }
  });
});

app.get('/fragebogen', function(req, res) { 

  fs.readFile('./uploads/' + req.auth.user + '.json', 'utf8', function (err, data) {
    if (err) {
      console.log(err);
    }
    else {

    res.end(data);
    }
  });
});

app.get('/fragebogens', function(req, res) { 
  generateYaml(res);
});

app.post('/fragebogen', function(req, res) {
  
  if (Object.keys(req.body).length == 0) {
    return res.status(400).send('No body was sent.');
  }

  fs.writeFile("./uploads/" + req.auth.user + ".json", 
    JSON.stringify(req.body, null, 4), function(err) {
    
    if ( err ) {
      console.log(err);
    }
    else {
      writeYaml();
      res.end();
    }
  });
});

function writeYaml() {

  fs.readdir('./uploads/', function(err, filenames) {

    if (err) {
      onError(err);
    }

    var allAnswers = [];
    filenames.forEach(function(filename) {
      if (filename.endsWith('.json')) {
        var data = JSON.parse(fs.readFileSync('./uploads/' + filename, 'utf-8')).data;
        data.id = data.vorname.toLowerCase() + "-" + data.nachname.toLowerCase();
        allAnswers.push(data);
      }
    });

    fs.writeFile("./_data/candidates.yml", YAML.stringify(allAnswers, 4), function(err) {
      
      if ( err ) {
        console.log(err);
      }
    });
  });  
}

function generateYaml(res) {

  var dirname = './uploads/';
  var allAnswers = [];

  fs.readdir(dirname, function(err, filenames) {

    if (err) {
      onError(err);
      return;
    }

    filenames.forEach(function(filename) {
      allAnswers.push(JSON.parse(fs.readFileSync(dirname + filename, 'utf-8')).data);
    });

    res.set('content-type', 'application/yaml');
    res.send(YAML.stringify(allAnswers, 4));
  });  
}

function readFiles(dirname, onFileContent, onError) {
  
  fs.readdir(dirname, function(err, filenames) {
    if (err) {
      onError(err);
      return;
    }
    filenames.forEach(function(filename) {
      fs.readFile(dirname + filename, 'utf-8', function(err, content) {
        if (err) {
          onError(err);
          return;
        }
        onFileContent(filename, content);
      });
    });
  });
}

app.listen(port, () => console.log(`Starting Fragebogen App auf Port ${port}!`))
