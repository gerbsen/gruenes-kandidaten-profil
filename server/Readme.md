# Server-Komponente

Starten des Servers mit automatischer Aktualisierung bei Änderungen:

```bash
nodemon index.js
```

## Funktionsweise

TODO

## Authentifizierung

Wir managen eine `users.csv` in der Grünen Wolke.
Diese Datei wird jede Minute auf den Server aus der Wolke geladen und aktualisiert.
Die Datei darf keinen Header haben und ist wie folgt aufgebaut:

```csv
username,password,email
```

Hier ein Beispiel:

```csv
daniel-gerber,123,email@email.com
```

## Docker

Für die Staging Umgebung auf dem Server:

```bash
docker run --name kommunalwahl_leipzig_fragebogen_dev \
    --restart always \
    -d -p 8013:8000 \
    -v /var/kommunalwahl_leipzig/users.csv:/usr/src/app/users.csv \
    -v /var/kommunalwahl_leipzig/develop/uploads:/usr/src/app/uploads \
    -v /var/kommunalwahl_leipzig/develop/_data:/usr/src/app/_data \
    registry.gitlab.com/gerbsen/gruenes-kandidaten-profil/fragebogen:develop
```
