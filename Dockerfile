FROM jekyll/builder:3.0.2 as builder

WORKDIR /usr/src/app
COPY . .

RUN bundle install 
CMD jekyll serve

EXPOSE 4000

# FROM nginx:1-alpine
# COPY --from=builder /usr/src/app/_site /usr/share/nginx/html