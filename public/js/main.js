$( document ).ready(function() {
    
    var images = ['leipzig-mdr-tower-cut.jpg', 'bridges-525620_1920.jpg', 'leipzig-1201195_1920.jpg', 'leipzig-2218521_1920.jpg', 'leipzig-745365_1920.jpg', 'leipzig-mdr-tower-cut.jpg', 'supreme-administrative-court-2390909_1920.jpg', 'travel-3094193_1920.jpg'];
    var randomIndex = Math.floor(Math.random() * images.length);
    $('#home').css({'background-image': 'url(public/images/backgrounds/' 
        + images[randomIndex] + ')'});

    [0,1,2,3,4,5,6,7,8,9].forEach(element => {

        if ( $('#map-' + element).length > 0 ) {

            var map = L.map('map-' + element).setView([51.33962, 12.37129], 11);
            L.tileLayer('https://maps.targomo.com/styles/osm-bright-gl-style/rendered/{z}/{x}/{y}.png', {
                attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.targomo.com/">Targomo</a>',
                maxZoom: 18
            }).addTo(map);

            var wahlkreis = L.geoJSON(wahlkreise, { 
                filter: function(feature, layer) {
                    return feature.properties.SBZ == element;
                },
                // more options can be found here: https://leafletjs.com/reference-1.4.0.html#path-option
                style: function (geoJsonFeature) {
                    return { 
                        'color' : "#e6007e",
                        'fill' : false
                    }
                }
            });
            wahlkreis.addTo(map);
            map.fitBounds(wahlkreis.getBounds());
        }
    });    
});


