#############################
## Front Matter            ##
#############################

.PHONY:

default: help

#############################
## Targets                 ##
#############################

## Start the presentation server in monitor mode
start-presentation:
	bundle exec jekyll serve

## Start the questionnaire server in monitor mode
start-questionnaire:
	cd server; ./node_modules/nodemon/bin/nodemon.js start.js

## Install project dependencies
install:
	bundle install
	cd server; npm install

#############################
## Help Target             ##
#############################

## Show help
help:
	@printf "Available targets\n\n"
	@awk '/^[a-zA-Z\-\_0-9]+:/ { \
		helpMessage = match(lastLine, /^## (.*)/); \
		if (helpMessage) { \
			helpCommand = substr($$1, 0, index($$1, ":")-1); \
			helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
			printf "%-20s %s\n", helpCommand, helpMessage; \
		} \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST)
